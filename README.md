# SamToAbundance

Transforms a set of SAM Files into a Abundance File of unique hits including unique mapped reads.
Reads can be filterd by quality when using the <  --QUALITY_THRESHOLD> <-q> parameter.
Process can be parallelized by using the <  --CPU_THREADS0> <t> parameter.
Input directory should be forwarded using the <  --INPUT> <i> parameter.
To filter the Noise, use <  --NOISE_THRESHOLD> <-n> by setting an int value.

The Environment needs to be set up using miniconda:

```
conda env create -f env/StA_environment.yml
```

access environment by using

```
conda activate StA_environment
```

The script can be run as follows:

```
python3 sam_to_abundance.py -i /path/to/data/ -q 20 -t 10 -n 3
```

Parameters:

```
-i / --INPUT    :Dierctory for INPUT
-q / --QUALITY_THRESHOLD   :Quality Threshold as int
-t / --CPU_THREADS    :Amount of Threads as int
-n / --NOISE_THRESHOLD  :Value to filter Noise and set values under threshold to 0 
```
