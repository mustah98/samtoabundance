import pandas as pd
import os
import subprocess as sp
import itertools
import threading
import multiprocessing
from multiprocessing import Pool
import sys
import pathlib
import time
import getopt

##########################
#arguments

def myfunc(argv):
    arg_input = ""
    arg_quality = ""
    arg_cpu = ""
    arg_noise = ""
    arg_help = "{0} -i <INPUT> -q <QUALITY_THRESHOLD> -t <CPU_THREADS> -n <NOISE_THRESHOLD>".format(argv[0])

    try:
        opts, args = getopt.getopt(argv[1:], "hi:q:t:n:", ["help", "INPUT = set input path for data",
        "QUALITY_THRESHOLD = set <int> value for quality filtering", "CPU_THREADS = set <int> value for parallelization", "NOISE_THRESHOLD = set <int> to exclude noise values by setting them to 0"])
    except:
        print(arg_help)
        sys.exit(2)

    for opt, arg in opts:
        if opt in ("-h", "--help"):
            print(arg_help)  # print the help message
            sys.exit(2)
        elif opt in ("-i", "--INPUT"):
            arg_input = arg
        elif opt in ("-q", "--QUALITY_THRESHOLD"):
            arg_quality = arg
        elif opt in ("-t", "--CPU_THREADS"):
            arg_cpu = arg
        elif opt in ("-n", "--NOISE_THRESHOLD"):
            arg_noise = arg

    print('INPUT:', arg_input)
    print('QUALITY_THRESHOLD:', arg_quality)
    print('CPU_THREADS:', arg_cpu)
    print('NOISE_THRESHOLD:', arg_noise)

    return arg_input, int(arg_quality), int(arg_cpu), int(arg_noise)

###########################
#loading animation

def animate():
    for c in itertools.cycle(['|', '/', '-', '\\']):
        if done:
            break
        sys.stdout.write('\rloading ' + c)
        sys.stdout.flush()
        time.sleep(0.1)
    sys.stdout.write('\rDone!     ')

#############################


def iterate_files(list):

    INPUT = list[0]
    file = list[1]
    all_unique_hits = list[2]
    amount_per_sample = []
    for hit in all_unique_hits:
        command =  "grep " + hit + " " + INPUT + file + " | wc -l"
        amount = float(sp.getoutput(command))-1
        amount_per_sample.append(int(amount))
    print(file)
    print(len(amount_per_sample))
    table_dict[(file.split(".")[0]).split("_")[2]] = amount_per_sample
    output = amount_per_sample
    return [(file.split(".")[0]).split("_")[2], output]




if __name__ == "__main__":

    start_time = time.time()

    # Global Variables
    INPUT, QUALITY_THRESHOLD, CPU_THREADS, NOISE_THRESHOLD = myfunc(sys.argv)

    done = False
    t = threading.Thread(target=animate)
    t.start()

    ABS_PATH = str(pathlib.Path().resolve()) + "/"
    RESULTS = ABS_PATH + "../results/Abundance/"
    TAXA = ABS_PATH + "../taxa/taxa_table.tsv"

    if os.path.exists(RESULTS):
        if len(os.listdir(RESULTS)) > 0:
            print("Clearing Past Results" + "\n")
            os.system("rm " + RESULTS + "*")
    else:
        os.system("mkdir -p " +  RESULTS)


    print("Prepatate Files ..." + "\n")
    sam_files = os.listdir(INPUT)
    file_names = [(file.split(".")[0]).split("_")[2] for file in sam_files]

    print("Detect Unique Hits ..." + "\n")
    all_hits = []
    for file in sam_files:
        command = "awk " +  "'!/LN/ && !/VN/ && $6 > " + str(QUALITY_THRESHOLD) + " {print $4}' " + INPUT + file
        unique_hits = list(set(sp.getoutput(command).split("\n")))
        all_hits += unique_hits

    all_unique_hits = list(set(all_hits))
    print("Number of unique hits = " + str(len(all_unique_hits)))

    table_dict = {
        "#OTU ID" : all_unique_hits #[0:6]

    }
    for file in file_names:
        table_dict.update({file : []})

    table_dict.update({"taxonomy" : []})


    taxa_list = []

    print("Extract Taxa Information from Hits ..." + "\n")
    for hit in all_unique_hits:
        command_taxa = "awk '$1 == " + hit + "{print}' " +  TAXA
        taxa = " ".join(sp.getoutput(command_taxa).split("\t")[1:])
        taxa_list.append(taxa)
    table_dict["taxonomy"] = taxa_list

    print("Extract Unique Hits From Sam Files ..." + "\n")

    ### parallelization ###
    if int(CPU_THREADS) > int(multiprocessing.cpu_count() - 1):
        print("More Threads assignet then system carries - Threads set to "  +  str(multiprocessing.cpu_count() - 1) + "\n")
        CPU_THREADS = multiprocessing.cpu_count() - 2



    params = [[INPUT, file, all_unique_hits] for file in sam_files]
    pool = Pool(CPU_THREADS)
    results = pool.map(iterate_files, params)


    print("Write Table to Output ..." + "\n")
    for i in range(len(results)):
        table_dict.update({results[i][0] : results[i][1]})
    print(table_dict)

    with open(RESULTS + "dict_save.tsv", 'w') as output_file:
        print(table_dict, file=output_file)

    df = pd.DataFrame.from_dict(table_dict)
    print(df.dtypes)
    # df = pd.to_numeric(df, errors='coerce').astype('int64')
    for column in list(df.columns):
        if column == "taxonomy" or column == "#OTU ID":
            continue
        else:
            df.loc[df[column] < NOISE_THRESHOLD, column] = 0

    df.to_csv(RESULTS + "abundance_table.tsv", sep = "\t" , index = False)

    done = True

    print("FINISHED in " , (time.time() - start_time) / 60, "minutes!")
