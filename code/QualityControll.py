import pandas as pd
import os
import multiprocessing
from multiprocessing import Pool
import numpy as np


CPU_THREADS = 32

file_path = "/scratch1/silver/mf2/mf2/"
results_path = "/scratch1/mustafa/rna-seq-projects/16_S_Bachelor_MF-2/Workflow/samtoabundance/results/"


files = os.listdir(file_path)
files = sorted(files)

files_1 = [file for file in files if file.split("_")[-2] == "R1" ]
files_2 = [file for file in files if file.split("_")[-2] == "R2" ]



os.system("mkdir " + results_path + "trimmed_sequences")


def trimming(list):
    path = list[0]
    file_1 = list[1]
    file_2 = list[2]

    print(file_1 + "##############" + file_2)

    out_1 = list[1]
    out_2 = list[2]

    trimm_parameters = " --detect_adapter_for_pe --cut_right --cut_window_size 3 --cut_mean_quality 20 --length_required 75 --thread 1 --n_base_limit 0"
    operation = "fastp -i " + file_path + file_1 + " -I " + file_path + file_2 + " -o " + results_path + "trimmed_sequences/" + out_1 + " -O " + results_path + "trimmed_sequences/" + out_2 + trimm_parameters
    os.system(operation)



## parallelization ##

params = [[file_path, files_1[i], files_2[i]] for i in range(int(len(files)/2))]


if len(files) < 32:
    pool = Pool(len(files))
else:
    pool = Pool(CPU_THREADS)


results = pool.map(trimming, params)
